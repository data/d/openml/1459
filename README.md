# OpenML dataset: artificial-characters

https://www.openml.org/d/1459

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: H. Altay Guvenir, Burak Acar, Haldun Muderrisoglu    
**Source**: [UCI](https://archive.ics.uci.edu/ml/datasets/Artificial+Characters) - 1992  
**Please cite**: [UCI](https://archive.ics.uci.edu/ml/citation_policy.html)  

This database has been artificially generated. It describes the structure of the capital letters A, C, D, E, F, G, H, L, P, R, indicated by a number 1-10, in that order (A=1,C=2,...). Each letter's structure is described by a set of segments (lines) which resemble the way an automatic program would segment an image. The dataset consists of 600 such descriptions per letter. 

Originally, each 'instance' (letter) was stored in a separate file, each consisting of between 1 and 7 segments, numbered 0,1,2,3,... Here they are merged. That means that the first 5 instances describe the first 5 segments of the first segmentation of the first letter (A). Also, the training set (100 examples) and test set (the rest) are merged. The next 7 instances describe another segmentation (also of the letter A) and so on.

### Attribute Information  

* V1: object number, the number of the segment (0,1,2,..,7)  
* V2-V5: the initial and final coordinates of a segment in a cartesian plane (XX1,YY1,XX2,YY2).  
* V6: size, this is the length of a segment computed by using the geometric distance between two points A(X1,Y1) and B(X2,Y2).
* V7: diagonal, this is the length of the diagonal of the smallest rectangle which includes the picture of the character. The value of this attribute is the same in each object.

### Relevant Papers  

M. Botta, A. Giordana, L. Saitta: "Learning Fuzzy Concept Definitions", IEEE-Fuzzy Conference, 1993.  
M. Botta, A. Giordana: "Learning Quantitative Feature in a Symbolic Environment", LNAI 542, 1991, pp. 296-305.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1459) of an [OpenML dataset](https://www.openml.org/d/1459). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1459/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1459/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1459/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

